#ifndef LIB_D_HEADER_INCLUDED
#define LIB_D_HEADER_INCLUDED

#include <string>

namespace ver
{
  extern std::string const VERSION;
}

#endif
