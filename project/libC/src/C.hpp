#ifndef LIB_C_HEADER_INCLUDED
#define LIB_C_HEADER_INCLUDED

#include "A.hpp"
#include "B.hpp"

class C
{
public:
  void run();

private:
  A _a;
  B _b;
};

#endif
