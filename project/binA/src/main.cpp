#include <iostream>

#include "A.hpp"
#include "D.hpp"

int main()
{
  A a;
  std::cout
      << "binA: running for binB version "
      << ver::VERSION
      << std::endl;
  a.print();
  return 0;
}
