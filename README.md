# CMake demo

A CMake demo project in C++.

A simple example with libraries, binaries, dependecies and generated code.

## Description
 
* Libraries:
    * libA
    * libB
    * libC: depends on libA and libB
    * libD: generated code
* Binaries:
    * binA: depends on libA and libD
    * binB: depends on libC and libD

## Test

From the project directory:

```bash
make && make run
```
