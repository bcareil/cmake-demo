#include <iostream>

#include "C.hpp"
#include "D.hpp"

int main()
{
  C c;
  std::cout << "binB version " << ver::VERSION << std::endl;
  c.run();
  return 0;
}
